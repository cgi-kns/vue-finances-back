const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

//instalar e importar moment pra validar a data
const moment = require('moment')

const { getUserId } = require('./../utils')

const JWT_SECRET = process.env.JWT_SECRET

//Mutation de Criação de contas (Dinheiro, saldos, valores)
function createAccount (_, {description}, ctx, info){
    const userId = getUserId(ctx)
    return ctx.db.mutation.createAccount({
        data: {
            description,
            user: {
                connect: {
                    id: userId
                }
            }
        }
    }, info)
}

//Mutation de Criação de Categorias
function createCategory (_, {description, operation}, ctx, info){
    const userId = getUserId(ctx)
    return ctx.db.mutation.createCategory({
        data: {
            description,
            operation,
            user: {
                connect: {
                    id: userId
                }
            }
        }
    }, info)
}

//Mutation de Criação de Registro
function createRecord (_, args, ctx, info){

    const date = moment(args.date) // vou receber formato 2020-11-18
    if (!date.isValid()) {
        throw new Error('Invalid date!')
    }

    let { amount, type } = args
    if ((type === 'DEBIT' && amount > 0) || (type === 'CREDIT' && amount < 0)) {
        amount = -amount
    }

    const userId = getUserId(ctx)
    return ctx.db.mutation.createRecord({
        data: {
            user: {
                connect: {id: userId}
                }, 
            account: {
                connect: {id: args.accountId}   
            },     
            category: {
                connect: {id: args.categoryId}   
            },   
            //...args <- outra alternativa  
            amount,
            type,
            date: args.date,
            description: args.description,
            tags: args.tags,
            note: args.tags
        }
    }, info)
}

//Mutation de Login
async function login (_, {email, password}, ctx, info) {

    const user = await ctx.db.query.user({where: {email}})
    if (!user){
        throw new Error('Invalid credentials!')
    }
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
        throw new Error('Invalid credentials!')
    }

    const token = jwt.sign({ userId: user.id }, JWT_SECRET, { expiresIn: '2h' })

    return {
        token,
        user
    }

}

//Mutation de Cadastro
async function signup (_, args, ctx, info) {

    const password = await bcrypt.hash(args.password, 10)
    const user = await ctx.db.mutation.createUser({ data: { ...args, password }})

    const token = jwt.sign({ userId: user.id }, JWT_SECRET, { expiresIn: '2h' })

    return {
        token,
        user
    }
}


//Exportação das Mutations
module.exports = {
    createAccount,
    createCategory,   
    createRecord,
    login,
    signup
}